#!/bin/bash

username=vagrant
# ssh_key_path=~/.ssh/profissional/id_rsa
ssh_key_path=/home/diego/AmbienteDev/profissional/NOMAD/cluster-nomad-local/.vagrant/machines/vm01/virtualbox/private_key


servers=(10.0.0.101)
# servers=(
#     server1.example.com
#     server2.example.com
#     server3.example.com
# )

# Loop through the servers and copy the SSH key
for server in "${servers[@]}"; do
    echo "Copiando chave para servidor $server..."
    ssh-copy-id -i $ssh_key_path $username@$server
done

#  ssh-copy-id -i ~/.ssh/pessoal/gitlab/id_rsa -o PreferredAuthentications=teste diego@10.0.0.101
