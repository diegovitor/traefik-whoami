#!/bin/bash

NEW_USER=suporteddsi

echo "etapa 1 - Criar usuário ${NEW_USER}"
useradd -m -s /bin/bash ${NEW_USER}
echo "${NEW_USER}:teste" | chpasswd

echo "etapa 2 - Criar diretórios e aplicar permissões para o usuário ${NEW_USER}"
mkdir -p /home/${NEW_USER}/.ssh

echo "etapa 3 - Atribuir permissões a pasta ssh"
chown -R ${NEW_USER}:${NEW_USER} /home/${NEW_USER}/.ssh
chmod 700 /home/${NEW_USER}/.ssh

echo "etapa 4 - Criar arquivo com permissões de root para usuário ${NEW_USER}"
touch /home/${NEW_USER}/.ssh/authorized_keys

echo "etapa 5 - Atribuir permissões ao arquivo authorized_keys"
chown -R ${NEW_USER}:${NEW_USER} /home/${NEW_USER}/.ssh/authorized_keys
chmod 600 /home/${NEW_USER}/.ssh/authorized_keys

echo "etapa 6 - Criar arquivo com permissões de root para usuário ${NEW_USER}"
cat <<EOF >/etc/sudoers.d/${NEW_USER}
${NEW_USER}  ALL=(ALL) NOPASSWD:ALL
EOF

echo "etapa 7 - Importar chave publica para o arquivo authorized_keys"
cat <<EOF >/home/${NEW_USER}/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkwePclV3WjSyDo4qKw5dK45uI6Vt8TcSZfWFOhMH7NpyPDLAoZnJ4zrGsILLNlXj1LwN2RVyCZyUa/74/wRJEd2z6pVdHcbl9WdVu3FvQCG7AAytq5aY5ZwzN5eomFWDitXw37p1gDpmooyJ6B7l7sK9MCz8vmEKpHN9lv/2yEwKOlpLGvBagJXz/aenWWsIE/qgGalNTKGrY+Wo1p/Izx8PQD26cZlzmBy/fyoO1IlBRGdWYsUJGcpgGAi2dJDNIUg+1Ppi+ZsT/F7znfgkCvqHrBof8SQ+Bx0treNPmur3CcJpi/GcazihHQtag1xphASBHheQ7RMcJG4sXTKKpCRRGGk4pGO1zH3T6G4oZ3yt61xViazQSFspAm1NeRWIdTAeHNg3EIulSJv1FcD6vMW+ndda5oZFOV49nrA+FcbhEew0EPjwdHe+oWQHXAltul7oD1d/4N26tMqPbb+A6iCmVUyXyQnKqpUCT+uZy6MzM+BY0fTEfAfHsJyOT3yM= diego@acer
EOF