<<comment
Passos para executar o script de teste do ansible
o script deve ser executado manualmente uma 
comment

# SSH_DIR=$PWD/ssh
# ssh-agent bash
# ssh-add $SSH_DIR/id_rsa

#Passo 1 - Executar o agente para o shell zsh
exec ssh-agent zsh

#Passo 2 - Adicionar a chave ao agente
ssh-add ./scripts/ssh/id_rsa

#Passo 3 - Realizar ping em todas as máquinas
ansible all -i inventory.ini -m ping

#Passo 4 - Realizar a instalação do cluster nomad-consul
ansible-playbook teste_cluster.yml -i inventory.ini