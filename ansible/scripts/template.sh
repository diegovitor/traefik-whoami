#!/bin/bash

NEW_USER=suporteddsi

echo "etapa 1 - Criar usuário ${NEW_USER}"
useradd -m -s /bin/bash ${NEW_USER}
echo "${NEW_USER}:teste" | chpasswd

echo "etapa 2 - Criar diretórios e aplicar permissões para o usuário ${NEW_USER}"
mkdir -p /home/${NEW_USER}/.ssh

echo "etapa 3 - Atribuir permissões a pasta ssh"
chown -R ${NEW_USER}:${NEW_USER} /home/${NEW_USER}/.ssh
chmod 700 /home/${NEW_USER}/.ssh

echo "etapa 4 - Criar arquivo com permissões de root para usuário ${NEW_USER}"
touch /home/${NEW_USER}/.ssh/authorized_keys

echo "etapa 5 - Atribuir permissões ao arquivo authorized_keys"
chown -R ${NEW_USER}:${NEW_USER} /home/${NEW_USER}/.ssh/authorized_keys
chmod 600 /home/${NEW_USER}/.ssh/authorized_keys

echo "etapa 6 - Criar arquivo com permissões de root para usuário ${NEW_USER}"
cat <<EOF >/etc/sudoers.d/${NEW_USER}
${NEW_USER}  ALL=(ALL) NOPASSWD:ALL
EOF

echo "etapa 7 - Importar chave publica para o arquivo authorized_keys"
cat <<EOF >/home/${NEW_USER}/.ssh/authorized_keys
${PUBLIC_KEY_VALUE}
EOF