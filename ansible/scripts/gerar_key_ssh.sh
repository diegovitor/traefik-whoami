<<comment
-N ""diz para ele usar uma senha vazia (a mesma que duas entradas em um script interativo)
-f my.keydiz para armazenar a chave my.key(altere conforme achar necessário).
- o -q é para silêncio

$ ssh-keygen -t rsa -b 4096 -C "comment" -P "examplePassphrase" -f "desired pathAndName" -q 

volssh-keygen -q -t rsa -N '' >/dev/null
O redirecionamento para nulo é necessário para silenciar a mensagem de substituição.
comment

SSH_DIR=$PWD/ssh

rm -rf $SSH_DIR
mkdir $PWD/ssh

ssh-keygen -t rsa -f $SSH_DIR/id_rsa
# ssh-keygen -t rsa -N "teste" -f $SSH_DIR/id_rsa

export PUBLIC_KEY_VALUE=$(cat $SSH_DIR/id_rsa.pub)

envsubst '$PUBLIC_KEY_VALUE' < template.sh > "script_add_user.sh"
# ssh-agent bash
exec ssh-agent zsh
ssh-add $SSH_DIR/id_rsa
